let data = require('./poll.json')
let r = require('rethinkdb')

let DB = 'SCMP'

async function main () {
  console.log('Connect')
  let conn = await r.connect()

  console.log('Delete DB')
  await r
    .dbList()
    .contains(DB)
    .do(function(databaseExists) {
      return r.branch(databaseExists, r.dbDrop(DB), null)
    })
    .run(conn)
    .catch(err => console.log(err))

  console.log('Create DB: ', DB)
  await r.dbCreate(DB).run(conn)

  console.log('Use: ', DB)
  conn.use(DB)

  console.log('Create Tables')
  await Promise.all([
    r.tableCreate('Polls').run(conn),
    r.tableCreate('Votes').run(conn)
  ]).catch(err => console.log(err))

  console.log('Insert Data')
  await r.table('Polls').insert(data.polls).run(conn)

  await Promise.all(data.polls.map(poll => {
    let data = poll.answer.options.map(o => ({ name: o.label, value: 0, id: o.id }))
    return r.table('Votes').insert({ total: 0, data, id: poll.id }).run(conn)
  }))

  console.log('Finised')
  conn.close()
}


main ()