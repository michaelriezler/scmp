# Setup

## Install
- node.js

## DB
### Using docker-compose
- run `docker-compose up`

### Without Docker
[Follow the guide on the rethinkdb website](https://www.rethinkdb.com/docs/install/)

## Start the server
After you have installed nodejs and rethinkdb is running

### Before you start the server for the first time
1. `npm install`
2. `npm run seed` to generate the database structure and insert the polls

### Start the server
- run `npm start`