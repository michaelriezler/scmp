let express = require('express')
let bodyParser = require('body-parser')
let r = require('rethinkdb')

let run = async (query) => {
  let conn = await r.connect({ db: 'SCMP' })

  try {
    let result = await query.run(conn)
    await conn.close()
    return result
  } catch (err) {
    conn.close()
    return err
  }
}

let Polls = () => r.table('Polls')
let Votes = () => r.table('Votes')

let PORT = 4000

let app = express()

app.use(bodyParser.json())

let api = express.Router()

api.get('/polls', async (req, res) => {
  let polls = await run(Polls().coerceTo('array'))
  res.json(polls)
})

api.get('/polls/:id', async (req, res) => {
  let id = parseInt(req.params.id)
  let poll = await run(Polls().get(id))
  res.json(poll)
})

api.get('/votes/:poll_id', async (req, res) => {
  let id = parseInt(req.params.poll_id)
  let votes = await run(Votes().get(id).default([]))
  res.json(votes)
})

api.post('/votes/:id', async (req, res) => {
  let id = parseInt(req.params.id)
  let { selected } = req.body
  let query = Votes().get(id).update(vote => {
    return {
      total: vote('total').add(1),
      data: vote('data').map(option => {
        return r.branch(r.contains(selected, option('id')),
          {
            name: option('name'),
            id: option('id'),
            value: option('value').add(1)
          },
          option
        )
      })
    }
  }, { returnChanges: true })

  let { changes } = await run(query)
  res.json(changes[0].new_val)
})

app.use('/api', api)

app.listen(PORT, () => console.log(`Server is running on port: ${PORT}`))