import React from 'react'
import styled from 'styled-components'
import Colors from '../Colors'

export let Answer = styled.input`
  display: none;
`

let AnswerLabel = styled.label`
  display: inline-block;
  border: 1px solid ${p => p.selected ? 'green' : Colors.get(p.index)};
  background: ${p => Colors.get(p.index)};
  padding: 4px 8px;
  margin-bottom: 8px;
  color: #fff;
  font-weight: 700;
`

let StyledAnswers = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`

let Answers = props => {

  let onChange = ({ type, value }) => (e) => {
    let { selected } = props
    let newState = []

    if (type === 'Single') {
      newState = [value]
    }

    if (type === 'Multi') {
      newState = selected.includes(value)
        ? selected.filter(id => id !== value)
        : [...selected, value]
    }

    props.onChange && props.onChange(newState)
  }

  let children = React.Children.map(props.children, 
    node => React.cloneElement(node, {
      id: `option-${props.name}-${node.props.value}`,
      name: props.name,
      type: node.props.type === 'Multi' ? 'checkbox' : 'radio',
      onChange: props.disabled ? null : onChange(node.props),
      selected: props.selected.includes(node.props.value)
    })
  )

  return (
    <StyledAnswers>
      { 
        children.map((c, index) =>
          <AnswerLabel
            index={index}
            key={c.props.id}
            htmlFor={`option-${props.name}-${c.props.value}`}
            selected={c.props.selected}>
            {c}
            {c.props.label}
          </AnswerLabel>
        )
      }
    </StyledAnswers>
  )
}

export default Answers