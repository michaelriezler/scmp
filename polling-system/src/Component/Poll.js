import React, { useState } from 'react'
import styled from 'styled-components'
import Answers, { Answer } from './Answers'
import { PieChart, Pie, Tooltip, Cell } from 'recharts'
import { format } from 'date-fns'
import axios from 'axios'
import Colors from '../Colors'

let StyledPollDate = styled.span``

export let PollDate = ({ children = new Date() }) => {
  let date = format(children, 'DD MMM YYYY')
  return <StyledPollDate>{date}</StyledPollDate>
}

let PollContent = styled.div``

let Votes = styled.p`
  width: 100%;
  text-align: center;
`

let ChartWrapper = styled.div`
  display: flex;
  justify-content: center;
`

export default ({
  id = null,
  title = '',
  date,
  options = [],
  votes,
  answer,
  onChange
}) => {

  let [selected, setSelected] = useState([])
  let [submited, setSubmited] = useState(false)

  let onSend = () => {
    axios.post(`/api/votes/${id}`, { selected }).then(res => {
      onChange && onChange(res.data)
      setSubmited(true)
    })
  }

  let showSend = submited === false && selected.length > 0 

  return (
    <PollContent>
      <h1>{title}</h1>{date && <PollDate>{date}</PollDate>}
      <Answers
        name={id}
        selected={selected}
        onChange={ids => setSelected(ids)}
        disabled={submited}>
        { options.map(o => <Answer key={o.id} type={answer.type}  value={o.id} label={o.label} />) }
      </Answers>

      { showSend && <button onClick={onSend}>Send</button> }

      <ChartWrapper>
        <PieChart width={200} height={200}>
          <Pie
            data={votes.data}
            cx={100}
            cy={100}
            outerRadius={50}
            fill="#8884d8"
            label
            dataKey='value'>
            { votes.data.map((_, index) => <Cell fill={Colors.get(index)} />)}
          </Pie>
          <Tooltip/>
         </PieChart>
      </ChartWrapper>

      <Votes>Totla number of votes recorded: { votes.total }</Votes>
    </PollContent>
  )
}