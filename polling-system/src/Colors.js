let Colors = [
  '#ffb743',
  '#001871',
  '#138d90',
  '#e95b4f',
  '#43bcff',
]

export default {
  get: index => Colors[index % Colors.length]
}