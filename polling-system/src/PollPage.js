import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import useVote from './useVote'
import axios from 'axios'
import Poll from './Component/Poll'

let PageWrapper = styled.div`
  background: lightblue;
  padding: 16px;
  min-height: calc(100vh - 40px);
`

let Header = styled.div`
  height: 40px;
  background: #fff;
  display: flex;
  align-items: center;
  padding: 0 16px;
`

export default ({ match, history }) => {
  let { id } = match.params
  let [poll, setPoll] = useState({})

  useEffect(() => {
    axios.get(`/api/polls/${id}`).then(res => setPoll(res.data))
  }, [id])

  let options = poll.answer && poll.answer.options

  let [votes, setVotes] = useVote(id)
  let onChange = data => {
    setVotes(data)
  }

  return (
    <React.Fragment>
      <Header>
        <span onClick={() => history.goBack()}>Back</span>
      </Header>
  
      <PageWrapper>
  
        <Poll
          id={poll.id}
          title={poll.title}
          options={options || []}
          answer={poll.answer}
          votes={votes}
          onChange={onChange}
        ></Poll>
      </PageWrapper>
    </React.Fragment>
  )
}