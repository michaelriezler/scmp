import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import PollListing from './PollListing'
import PollPage from './PollPage'

let App = () => {
  return (
    <Switch>
      <Route path='/poll/:id' component={PollPage} />
      <Route path='/' component={PollListing} />
      <Redirect to='/' />
    </Switch>
  )
}
export default App
