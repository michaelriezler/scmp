import { useState, useEffect } from 'react'
import axios from 'axios'

let Votes = new Map()

export default pollId => {
  let initalState = Votes.has(pollId) ? Votes.get(pollId) : { total: 0, data: [] }
  let [votes, setVotes] = useState(initalState)

  useEffect(() => {
    if (pollId === undefined) return
      
    axios.get(`/api/votes/${pollId}`).then(res => {
      Votes.set(pollId, res.data)
      setVotes(res.data)
    })
  }, [pollId])

  return [votes, setVotes]
}