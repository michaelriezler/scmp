import { useState, useEffect } from 'react'
import axios from 'axios'

let Polls = new Map()

let usePolls = () => {
  let [data, setPolls] = useState({ polls: Polls, loading: true })

  useEffect(() => {
    if (data.polls.size > 0) return

    axios.get('/api/polls').then(res => {
      let polls = new Map()

      res.data.forEach(poll => polls.set(poll.id, poll))

      setPolls({ polls, loading: false })
    })
  })

  return data
}

export default usePolls