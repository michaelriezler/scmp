import React from 'react'
import styled from 'styled-components'
import usePolls from './usePolls'
import useVote from './useVote'
import { Link } from 'react-router-dom'
import Poll, { PollDate } from './Component/Poll'

let StyledToday = styled.div`
  padding: 16px;
  background: #fafafa;
`

let TodaysPoll = ({
  id,
  title,
  publishedDate,
  answer = {}
}) => {
  let [votes, setVotes] = useVote(id)
  let { options = [] } = answer

  let onChange = data => setVotes(data)

  return (
    <StyledToday>
      <div>
        <h2>Today's Poll</h2>
      </div>
      <Poll
        id={id}
        title={title}
        date={publishedDate}
        options={options}
        answer={answer}
        votes={votes}
        onChange={onChange}
      ></Poll>
    </StyledToday>
  )
}


let Card = styled.div`
  border-bottom: 1px solid black;
`

let StyledLink = styled(Link)`
  color: #000;
  padding: 16px;
  text-decoration: none;
  display: block;

  :last-child ${Card} {
    border-bottom: none;
  }
`

export default () => {
  let { polls } = usePolls()
  let [latest, ...rest] = polls.values()

  return (
    <div>
      <TodaysPoll {...latest} />
      { 
        rest.map(p => 
          <StyledLink to={`/poll/${p.id}`} key={p.id}>
            <Card>
              <PollDate>{p.publishedDate}</PollDate>
              <h4>{p.title}</h4>
            </Card>
          </StyledLink>
        )
      }    
    </div>
  )
}