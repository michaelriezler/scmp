let express = require('express')
let bodyParser = require('body-parser')
let f = require('faker')

let PORT = 4000

let app = express()

app.use(bodyParser.json())


let api = express.Router()

api.get('/polls', (req, res) => {
  wait(() => res.json(Polls))
})

api.get('/polls/:id', (req, res) => {
  let id = parseInt(req.params.id)
  let poll = Polls.find(p => p.id === id)

  wait(() => res.json(poll))
})

api.get('/votes/:poll_id', (req, res) => {
  let id = parseInt(req.params.poll_id)
  let poll = Polls.find(p => p.id === id)

  let data = poll.answer.options.map(o => ({ name: o.label, value: 0, id: o.id }))

  let vote = Votes.has(id)
    ? Votes.get(id)
    : { total: 0, data }

  Votes.set(id, vote)

  wait(() => res.json(vote))
})

api.post('/votes/:id', (req, res) => {
  let id = parseInt(req.params.id)
  let { selected } = req.body

  let { total, data } = Votes.get(id)

  var newData = data.map(o => selected.includes(o.id)
    ? Object.assign(o, { value: o.value + 1 })
    : o
  )

  let newTotal = total + 1

  let vote = { total: newTotal, data: newData }
  Votes.set(id, vote)

  res.json(vote)
})

app.use('/api', api)

app.listen(PORT, () => console.log(`Server is running on port: ${PORT}`))


let wait = fn =>  setTimeout(fn, 500) 


/*
 * Data
*/

let Polls = [{
    "id": 1,
    "title": "Is bitcoin worth the time and money that mining requires?",
    "publishedDate": 1516605447,
    "answer": {
      "type": "Single",
      "options": [{
          "id": 1,
          "label": "Yes"
        },
        {
          "id": 2,
          "label": "No"
        }
      ]
    }
  },
  {
    "id": 2,
    "title": "Should chatbots replace humans in customer service jobs?",
    "publishedDate": 1516000647,
    "answer": {
      "type": "Single",
      "options": [{
          "id": 3,
          "label": "Yes"
        },
        {
          "id": 4,
          "label": "No"
        }
      ]
    }
  },
  {
    "id": 3,
    "title": "How are we feeling about 2018?",
    "publishedDate": 1515568647,
    "answer": {
      "type": "Single",
      "options": [{
          "id": 5,
          "label": "Hopeful"
        },
        {
          "id": 6,
          "label": "Doubtful"
        }
      ]
    }
  },
  {
    "id": 4,
    "title": "Which country/region have you ever visited? (Select all that applies)",
    "publishedDate": 1515482247,
    "answer": {
      "type": "Multi",
      "options": [{
          "id": 7,
          "label": "Hong Kong"
        },
        {
          "id": 8,
          "label": "China"
        },
        {
          "id": 9,
          "label": "Australia"
        },
        {
          "id": 10,
          "label": "Thailand"
        },
        {
          "id": 11,
          "label": "Korea"
        },
        {
          "id": 12,
          "label": "Japan"
        }
      ]
    }
  },
  {
    "id": 5,
    "title": "Will new benefits encourage you to study or work in mainland?",
    "publishedDate": 1515309447,
    "answer": {
      "type": "Single",
      "options": [{
          "id": 13,
          "label": "Yes"
        },
        {
          "id": 14,
          "label": "No"
        }
      ]
    }
  }
]


let Votes = new Map()